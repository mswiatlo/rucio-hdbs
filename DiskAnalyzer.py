#!/usr/bin/env python
# coding: utf-8

from rucio.client import Client

from datetime import datetime
from datetime import time
from datetime import timedelta

import json
import time as t
import pandas as pd
import numpy as np



def attachTag(df, database_file):


    print ('Attaching tag')
    start_time = t.time()
    with open(database_file, 'r') as analysis_database_file:
        analysis_database = json.load(analysis_database_file)

    tmp_df = pd.DataFrame()
    tmp_df['Tag'] = analysis_database.keys()
    tmp_df['Analysis'] = analysis_database.values()

    
    #df['Analysis'] = 'unknown'
    #print df['Analysis']                                                                                                                                                                                               

    #1. Find index and values corresponding to                                                                                                                                                                          
    #index, res  = np.where(tmp_df.Tag.apply(lambda x:df.name.str.contains(x)).T  == True)
    #print(index.shape, res.shape)
    #df.loc[index,'Analysis'] = tmp_df.Analysis[res].to_numpy()                                                                                                                                                        
    analysis = []
    for name in df['name']:
        found = False
        for tag,an in zip(tmp_df['Tag'], tmp_df['Analysis']):
            if tag in name: 
                analysis.append(an)
                found = True
                break
        if not found: analysis.append('unknown')             
    print(len(analysis))
    df['Analysis'] = analysis
    all_analysis = np.unique(list(analysis_database.values()))
    all_analysis = np.append(all_analysis, 'unknown')    
    print ('Done in ',t.time() - start_time, "seconds")
    return all_analysis


def getAnalysisQuota(df, analysis):
    #now get analysis quota                                                                                                                                                                                             
    analysis_quota = {}
    for a in analysis:
        size = df.loc[df['Analysis'] == a]['bytes'].sum()/1e9
        analysis_quota[a] = size

    sorted_database = sorted(((v, k)
                          for k, v in analysis_quota.items()), reverse=True)

    return sorted_database

def makeFileList(a, df):
    fileName = 'dataset_records/{}.txt'.format(a)
    outFile = open(fileName, 'w')
    
    if a in df['Analysis'].unique():
        for f in df.loc[df['Analysis'] == a]['name']:
            outFile.write(f + '\n')
    outFile.close()
    
def prepareFinalDatabase(df, analysis, createList = False):


    sizes = []
    files = []
    a_db = df['Analysis'].value_counts()
    for a in analysis:
       
        if not a in df['Analysis'].unique():
            sizes.append(0)
            files.append(0)
           
        else:
            sizes.append(df.loc[df['Analysis'] == a]['bytes'].sum()/1e9)
            files.append(a_db[a])
        if createList: makeFileList(a, df)
    df_final = pd.DataFrame()
    df_final['Analysis'] = analysis
    df_final['Quota'] = sizes
    df_final['Total Files'] = files

    #sanity check: checkf if total quota are the same

    #if  float(df_final['Quota'].sum()) != float(df['bytes'].sum()/1e9): 
    #    raise RuntimeError('Please check quotas!\n final df: {} -  initial df: {}. Difference is {}'.format(
    #        df_final['Quota'].sum(),  
    #        df['bytes'].sum()/1e9, 
    #        df_final['Quota'].sum()-df['bytes'].sum()/1e9) )
    return df_final.sort_values(by=['Quota'],ascending=False)

def expirationInfo(database_file,rse, alert_day = 21,makeExpirationList=False):

    print ('Expiration Info')
    #adding to dataframe rules id and expiration
    today = datetime.today()
    alert_diff = timedelta(days=alert_day)

    rules = []
    expiration = []

    filters={u'rse_expression': rse}
    columns = [u'name', u'scope', u'id',u'expires_at']
    
    #All columns
    #Index([u'account', u'activity', u'child_rule_id', u'comments', u'copies',
    #   u'created_at', u'did_type', u'eol_at', u'error', u'expires_at',
    #   u'grouping', u'id', u'ignore_account_limit', u'ignore_availability',
    #   u'locked', u'locks_ok_cnt', u'locks_replicating_cnt',
    #   u'locks_stuck_cnt', u'meta', u'name', u'notification', u'priority',
    #   u'purge_replicas', u'rse_expression', u'scope',
    #   u'source_replica_expression', u'split_container', u'state', u'stuck_at',
    #   u'subscription_id', u'updated_at', u'weight'],

    all_rules = Client.list_replication_rules(CLIENT,filters=filters)
    df_rules = pd.DataFrame(list(all_rules)[:-1], columns = columns)
    df_rules.to_csv('df_rulesAll.csv',index=True)
  
    #df_rules = pd.read_csv('df_rulesAll.csv',index_col=0)
    #converting to datetime
    df_rules['expires_at'] = pd.to_datetime(df_rules['expires_at'])
    
    #add days remaining to expiration
    df_rules['Days'] = df_rules['expires_at'].apply(lambda x: x - today if x!=None else 0)
    #dates = df_rules [df_rules['expires_at'].notnull()]['expires_at'].to_numpy()
    df_rules['Expire'] = df_rules['Days'].apply(lambda x : x < timedelta(days=alert_day) if x else False )
    
    analysis = attachTag(df_rules, database_file)

    #get expirations
    for a in analysis:
        if a in df_rules['Analysis'].unique():
            print (a, df_rules[df_rules['Analysis'] == a]['Days'].count())
        else:
            print(a,'0')


    #make expiration file list 
    if makeExpirationList:
        for a in analysis:
            fileName = 'dataset_records/expiring_{}.txt'.format(a)
            outFile = open(fileName, 'w')
            if a in df_rules['Analysis'].unique():
                df_ =  df_rules[ (df_rules['Days'].notnull()) & (df_rules['Expire']==True) & (df_rules['Analysis']==a)]
                for dset,exp in zip(df_['name'], df_['expires_at']):
                    outFile.write(dset + ' expires at '+ str(exp) + '\n')
                    
            outFile.close()



    return df_rules




def prepareTable(df, df_rules, grid_total_space = 300):
    quota_danger = 10000.
    quota_warning = 5000.
    exception_factor = 1.
    expiring_rules = 'DUMMY'
    sorted_string = '| *Analysis Team* | *Disk Usage* | *Total Files* | *Number of Rules Expiring in < 21 Days* |\n'
    exps = []
    
    for (name, size, files) in zip (df['Analysis'], df['Quota'], df['Total Files']):
        color = ''
        end_color = ''
        if size > (quota_danger * exception_factor):
            color = '%RED%'
        elif size > (quota_warning * exception_factor):
            color = '%ORANGE%'
        if color: end_color = '%ENDCOLOR%'
    
        expiring_rules = df_rules[ (df_rules['Days'].notnull()) & (df_rules['Expire']==True) & (df_rules['Analysis']==name)]['name'].count()
        
        sorted_string += '| [[https://gitlab.cern.ch/mswiatlo/rucio-hdbs/-/raw/master/dataset_records/{name}.txt][{name}]]         | {color} {size} GB {end_color}         | {files}         | [[https://gitlab.cern.ch/mswiatlo/rucio-hdbs/-/raw/master/dataset_records/expiring_{name}.txt][{exp} expiring rules]] | \n'.format(name = name,                                                                                                       
                                                                                                       color=color, size=size,
                                                                                                       end_color=end_color, 
                                                                                                       files=files, 
                                                                                                       exp=expiring_rules)
        

        exps.append(expiring_rules)
        #| {expiring_rules} | \n'

    today = datetime.today()
    d2 = today.strftime("%B %d, %Y")
    df['date'] = today
    df['date'] =  df['date'].dt.date
    df['expiring'] = exps
    
   
    total_size = df['Quota'].sum()
    sorted_string += '| *TOTAL* | *{:.2f}/{:.2f} Tb* | *{:.2f}* |\n'.format( (total_size/1000), grid_total_space, (100*(total_size/1000)/grid_total_space))
    sorted_string += '| *Last update* | *{update}* |  |\n'.format( update = d2 )
    #print (sorted_string)
    print ('Total usage is ' + str(total_size) + ' GB')
    print(sorted_string)
    with open('dataset_records/sizes.txt', 'w') as sizes_file:
        sizes_file.write(sorted_string)






if __name__ == '__main__':

    skipRucio = False

    start_time = t.time()
    CLIENT = Client()
    
    #Settings
    database_file = '../rucio-hdbs/analysis_database.json'
    rse='CERN-PROD_PHYS-HDBS'

    #Get all datasets in rse
    if not skipRucio:
        print ('Starting retrieving information')
        all_datasets = Client.list_datasets_per_rse( Client(), rse = rse)
        print ('Datasets retrieved in ', t.time() - start_time,' seconds')
        #Create dataframe
        columns = [ u'name', u'bytes',       u'created_at', u'length',u'scope',u'state']
        df = pd.DataFrame(list(all_datasets)[:-1], columns = columns)
        #df = pd.DataFrame(list(all_datasets)[:-1])
        print ('Data frame created')
        df.to_csv('df_All.csv',index=True)
    
        print(df)

    else:
        print('Opening csv')
        df = pd.read_csv('df_All.csv',index_col=0)

    #Attaching analysis tag                                                                                
    analysis = attachTag(df, database_file)
    print(df.tail)
    print (len(analysis))
    #analysis = np.append(analysis, 'unknown')
    print (analysis)

    print('Analysis quotas')
    analysis_database = getAnalysisQuota (df, analysis)                           
    tot_size = 0              
    for (size, name)  in analysis_database:                                                                
        print (name, size)
        tot_size += size                                                                                   
                                                                                                     
    print ('Total ', tot_size)
    print(df['bytes'].sum()/1e9)
    

    #create a final database
    df_final = prepareFinalDatabase(df, analysis, createList = True)
    print(df_final)


    rse='CERN-PROD_PHYS-HDBS'
    df_rules = expirationInfo(database_file, rse,makeExpirationList=True)

    #df_rules[df_rules['Days'].notnull() & df_rules['Expire']==True]['Analysis'].value_counts()#[['expires_at','Expire','Days']]
    #print(df_rules[ (df_rules['Days'].notnull()) & (df_rules['Expire']==True) & (df_rules['Analysis']=='VVJJ')]['name'].count())


    
    prepareTable(df_final, df_rules)
    
    
    df_final.to_csv('database/db_{}.csv'.format(datetime.today().strftime('%Y_%m_%d')))
    #df_final.to_csv('test.csv')
    #df_final.to_pickle('test.pkl')
