# rucio-hdbs: Useful scripts for monitoring HDBS grid space

Written by Max Swiatlowski and Francesco Cirotto

## What is this?
These scripts are used from HDBS grid space manager to monitor HDBS grid space usage. It creates the table shown in [HDBS twiki page](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/HDBSGridSpace).

HDBS grid space (CERN-PROD_PHYS-HDBS) has currently a quota of 300 Tb. Analysers can use the space to store ntuple production. More information in the [HDBS twiki page](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/HDBSGridSpace).


## Usage

### Adding analysis

The file [analysis_database.json](https://gitlab.cern.ch/mswiatlo/rucio-hdbs/-/blob/master/analysis_database.json) contains a dictionary where:

- keys are substring contained in a  full dataset name
- values are analysis names

For example:
```
 "VVSemi" : "VVsemileptonic"
```

Thus all datasets on CERN-PROD_PHYS-HDBS containing "VVSemi" string will be assigned to VVsemileptonic analysis. Analyses can have multiple keys (see above example for HH_bbtautau):

```
 "Pikachu": "HH_bbtautau",
 "Squirtle": "HH_bbtautau",
 "bbtt_hh": "HH_bbtautau",
```

### Monitoring HDBS space

The current script used for monitoring is [DiskAnalyzer.py](https://gitlab.cern.ch/mswiatlo/rucio-hdbs/-/blob/master/DiskAnalyzer.py). It used pandas and rucio, so a setup is needed:

```
source setup.sh
```

**IMPORTANT:** This setup may not worker always since new release and updates could create a conflict between pandas and rucio. It is important to find a setup which allows to use both packages since they're used in the same script.


Once setup is done, simply run 

```
python DiskAnalyzer.py
```

You will have several outputs on screen showing how space is taken from the analyses. For each analysis two text files are created/updated in `dataset_records` directory, containing the list of all datasets belonging to an analysis and expiration info (if set by the analyser who created replication on HDBS grid space). The text file [sizes.txt](https://gitlab.cern.ch/mswiatlo/rucio-hdbs/-/blob/master/dataset_records/sizes.txt) is the table you can see on the twiki page. Finally in `database` directory a csv file is created containing all information-

Once run is over, please update repository. This will also update table on twiki page.


### Plotting
The python notebook [DiskStats.ipynb](https://gitlab.cern.ch/mswiatlo/rucio-hdbs/-/blob/master/DiskStats.ipynb) creates summary plots. You can run the cells and then update (manually) plots on twiki page. The notebook just read csv files present in `database` directory to produce plots showing several information as function of time. 
