import os
import sys
from datetime import datetime
import pandas as pd
import time as t
import csv 
def size_to_unit(number,unit='auto'):
    exponents_map = {'B': 0, 'KB': 1, 'MB': 2, 'GB': 3, 'TB':4}
    if unit == 'auto':
        if number==0: return 0.0, 'B'
        for u in ['TB', 'GB', 'MB', 'KB', 'B']:
            size = number / 1024 ** exponents_map[u]
            if size >= 1:
                return round(size, 3), u

    elif unit not in exponents_map:
        raise ValueError("Must select from ['B', 'KB', 'MB', 'GB', 'TB']")

    size = number / 1024 ** exponents_map[unit]
    return round(size, 3), unit

def get_folder_size(path):
    yearwise_sizes = {}
    for dirpath, dirnames, filenames in os.walk(path):
        for filename in filenames:
            filepath = os.path.join(dirpath, filename)
            try:
                file_stats = os.lstat(filepath)
                
                modified_time = datetime.fromtimestamp(file_stats.st_mtime)

                file_year = modified_time.year
                file_size = file_stats.st_size

                if file_year not in yearwise_sizes:
                    yearwise_sizes[file_year] = {"size": 0, "nfiles": 0}

                yearwise_sizes[file_year]["size"] += file_size
                yearwise_sizes[file_year]["nfiles"] += 1

            except Exception as e:
                pass  # Handle if file not found
    return yearwise_sizes

def printsize(size,unit='auto'):
    number, unit = size_to_unit(size,unit)
    return str(number)+" "+unit

def prepareTable(info):
    sorted_string = '| *Analysis* | *Group* | *Usage [GB]* | *Number of Files* | *Old Usage (<=2021)* |\n'
    total_usage = 0
    total_files = 0 
    old_usage = 0
    to_sort = []
    for group, group_data in info.items():
        for analysis, years_data in group_data.items():
            total_size = sum(year_data["size"] for year_data in years_data.values())
            print(years_data, years_data.values(), total_size)
            old_size = sum(year_data["size"] for year, year_data in years_data.items() if year <= 2021)
            num_files = sum(year_data["nfiles"] for year_data in years_data.values())

            print(group,analysis,printsize(total_size),printsize(old_size),printsize(total_usage),printsize(old_usage), num_files)
            to_sort.append([total_size,f'| {analysis} | {group} | {printsize(total_size)} | {num_files} | {printsize(old_size)} | \n'])
            total_usage += total_size
            old_usage += old_size
            total_files += num_files
    
    to_sort = sorted(to_sort)
    for size,string in reversed(to_sort):
        sorted_string+=string
    sorted_string += f'| *TOTAL* | * * | *{size_to_unit(total_usage,"TB")[0]}/140.0 TB ({(size_to_unit(total_usage,"TB")[0]/140.0)*100:.2f}%)* | *{total_files}* | *{size_to_unit(old_usage,"TB")[0]}/140.0 TB ({(size_to_unit(old_usage,"TB")[0]/140.0)*100:.2f}%)* |\n '
    
    today = datetime.today().strftime("%B %d, %Y")
    sorted_string += f'| *Last update* | *{today}* |   |   |\n'

    print(sorted_string)
    with open('eos_database/sizes.txt', 'w') as sizes_file:
        sizes_file.write(sorted_string)

def ReadEos(base_path):
    def scan_analysis(info,group_dir,analysis_name,analysis_path):
        print(f"        scanning {analysis_name}")
        info[group_dir][analysis_name] = get_folder_size(analysis_path)
        totalforanalysis = sum([size_to_unit(info[group_dir][analysis_name][x]["size"],"GB")[0] for x in info[group_dir][analysis_name].keys()])
        totalfileforanalysis = sum([info[group_dir][analysis_name][x]["nfiles"] for x in info[group_dir][analysis_name].keys()])
        print("->",totalforanalysis, totalfileforanalysis, [str(x)+": "+str(size_to_unit(info[group_dir][analysis_name][x]["size"],"GB")[0]) + " " + str(info[group_dir][analysis_name][x]["nfiles"])  for x in info[group_dir][analysis_name].keys()])

    info = {}
    print(f"Scanning {base_path}")
    group_directories = os.listdir(base_path)
    print(f"found main directories: {','.join(group_directories)}")
    
    for group_dir in group_directories:
        print(f"    scanning {group_dir}")
        group_path = os.path.join(base_path, group_dir)
        info[group_dir]={}
        analysis_folders = os.listdir(group_path)
        print(f"    found sub-directories: {','.join(analysis_folders)}")
        for analysis_name in analysis_folders:
            analysis_path = os.path.join(group_path,analysis_name)

            # diHiggs Run3 exception
            if group_dir == 'diHiggs' and analysis_name == 'Run3':
                run3_subfolders = os.listdir(analysis_path)
                for subfolder in run3_subfolders:
                    subfolder_path = os.path.join(analysis_path, subfolder)
                    full_analysis_name = f"Run3/{subfolder}"
                    scan_analysis(info,group_dir,full_analysis_name,subfolder_path)
            else:
                scan_analysis(info,group_dir,analysis_name,analysis_path)
    return info

def WriteDataBase(info,output_directory):
    output_path = output_directory+'/db_{}.csv'.format(datetime.today().strftime('%Y_%m_%d'))
    with open(output_path, 'w', newline='') as ofile:
        csv_writer = csv.writer(ofile)
        csv_writer.writerow(['Group', 'Analysis Folder', 'Year', 'Space Used (GB)', 'Number of Files'])
        for ikey in info.keys():
            for jkey in info[ikey].keys():
                for year, aux in info[ikey][jkey].items():
                    csv_writer.writerow([ikey, jkey, year, aux["size"], aux["nfiles"]])

    print(f"CSV file saved to {output_path}")

def ReadDataBase(input_file):
    info = {}
    with open(input_file,'r') as ifile:
        csv_file = ifile.readlines()[1:]
        for line in csv_file:
            group, analysis, year, size, files = line.rstrip("\n").split(",")
            if group not in info.keys():
                info[group] = {}
            if analysis not in info[group].keys():
                info[group][analysis]={}
            info[group][analysis][int(year)]={"size": float(size), "nfiles": int(files)}
    return info

if __name__ == "__main__":
    start_Time = t.time()
    
    #Settings
    base_path = '/eos/atlas/atlascerngroupdisk/phys-hmbs/'
    output_directory = 'eos_database'

    input_file = None
    if len(sys.argv)>=2:
        input_file = sys.argv[1]

    info = {}    
    if input_file == None:
        info = ReadEos(base_path)
        print("Done in",t.time()-start_Time,"seconds")
        print(info) 
        WriteDataBase(info,output_directory)
    else:
        info = ReadDataBase(input_file)

    prepareTable(info)




    
