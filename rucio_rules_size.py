from rucio.client import Client
from datetime import datetime, date
CLIENT = Client()
blah = CLIENT.ping()

filters={u'rse_expression': u'CERN-PROD_PHYS-HDBS'}
        #   'created_after':  '01 JUL 2020'}

# start_date = datetime(2020, 6, 1, 0, 0, 0, 0)

start_date = datetime(2020, 6, 25, 0, 0, 0, 0)
end_date   = datetime(2020, 7, 5, 0, 0, 0, 0)

all_rules = Client.list_replication_rules(CLIENT,filters=filters)

# test_files = Client.list_files(CLIENT, 'group.phys-hdbs', 'group.phys-hdbs.304014.MultileptonFW2_v2_16a')
# for test_file in test_files:
#     print 'test'
#     print(test_file)

total_size = 0
names_list = ''
# for rule in all_rules:
#     scope = rule[u'scope']
#     date = rule[u'created_at']
#     # scope_test = ('group.phys-hdbs' in scope) or ('user.cdeutsch' in scope)
#     scope_test = True
#     # date_test = date > start_date
#     # date_test = (date > start_date) and (date < end_date)
#     date_test = True
#     if scope_test and date_test:
#         files = Client.list_files(CLIENT, rule[u'scope'], rule[u'name'])
#         # print 'new datasets'
#         for dataset in files:
#             # print(dataset[u'name'])
#             size = int(dataset[u'bytes'])
#             total_size += size

all_datasets = Client.list_datasets_per_rse(CLIENT,'CERN-PROD_PHYS-HDBS')

for dataset in all_datasets:
    date = dataset[u'created_at']
    date_test = (date > start_date) and (date < end_date) 
    date_test = True
    # name_test = 'Charizard' in dataset[u'name'] or 'Pikachu' in dataset[u'name'] or 'Squirtle' in dataset[u'name']
    name_test = 'user.brottler' in dataset[u'name']
    # name_test = True
    if name_test and date_test:
        total_size += dataset[u'bytes']
        names_list += dataset[u'name'] + ' ' + dataset[u'scope'] + '\n'
    # print(dataset)

print (total_size  / 1e9)

# with open('names.txt', 'w') as out_file:
#     out_file.write(names_list)



# print test
# first = next(test)
# print(first)

# print(first[u'name'])

# rule_info = Client.list_files(CLIENT, first[u'scope'], first[u'name'])


# for thing in rule_info:
#     print thing
